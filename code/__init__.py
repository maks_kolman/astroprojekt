# -*- coding: UTF-8 -*-
from collections import deque
import math
import random


def najdi_luno(img, svetlo=10):
    # To returna še neznano kaj.
    # Najde črnino okoli Lune (shranjeno v visited).
    # Najde rob Lune (Shranjeno v rob).
    # TODO: Dalo bi se še polepšati rob in najti dejansko Luno
    # (ne samo črnino in rob).
    
    if img.mode != 'L':
        img = img.convert('L')
    xdim, ydim = img.size
    visited = [[False for j in range(ydim)] for i in range(xdim)]
    rob = [[False for j in range(ydim)] for i in range(xdim)]
    
    if img.getpixel((0, 0)) > svetlo:
        raise Exception("Luna se dotika roba.")
        return
    
    vrsta = deque([(0, 0)])
    # To nikol ne znam v pythnu lepo spisat. A je treba heapq al kaj že nucat?
    while len(vrsta) > 0:
        i, j = vrsta.popleft()
        if visited[i][j]:
            continue
        if img.getpixel((i, j)) > svetlo:
            rob[i][j] = True
            continue
        visited[i][j] = True
        
        if i > 1:
            vrsta.append((i-1, j))
            if j > 1: vrsta.append((i-1, j-1))
            if j < ydim-2: vrsta.append((i-1, j+1))
        if i < xdim-2:
            vrsta.append((i+1, j))
            if j > 1: vrsta.append((i+1, j-1))
            if j < ydim-2: vrsta.append((i+1, j+1))
        if j > 1: vrsta.append((i, j-1))
        if j < ydim-2: vrsta.append((i, j+1))

    #POPRAVLJANJE ROBA V rob so tudi dislocirane enote, ki jih najde. Se
    #probamo znabiti tega. Najde en cikel. Če je dolžina tega cikla večja od
    #polovice celega roba, odstrani vse ostalo iz roba.
    while True:
        dolzina_roba = sum([sum(i) for i in rob])
        startx = 0
        starty = 0  # Predpostavlja, da ni roba na (0, 0)
        for i in range(len(rob)):
            for j in range(len(rob[i])):
                if rob[i][j]:
                    startx = i
                    starty = j
                    break
            if startx != 0 or starty != 0:
                break

        # Flodfill tega roba
        rob2 = [[False for j in range(ydim)] for i in range(xdim)]
        vrsta = deque([(startx, starty)])
        # To nikol ne znam v pythnu lepo spisat. A je treba heapq al kaj že nucat?
        while len(vrsta) > 0:
            i, j = vrsta.popleft()
            if rob2[i][j]:  # if visited
                continue
            if not rob[i][j]:
                continue
            rob2[i][j] = True
            
            if i > 1:
                vrsta.append((i-1, j))
                if j > 1: vrsta.append((i-1, j-1))
                if j < ydim-2: vrsta.append((i-1, j+1))
            if i < xdim-2:
                vrsta.append((i+1, j))
                if j > 1: vrsta.append((i+1, j-1))
                if j < ydim-2: vrsta.append((i+1, j+1))
            if j > 1: vrsta.append((i, j-1))
            if j < ydim-2: vrsta.append((i, j+1))
        
        nova_dolzina_roba = sum([sum(i) for i in rob2])
        if dolzina_roba == nova_dolzina_roba: break #Če je rob iz enega dela, prekini.
        if dolzina_roba/2 < nova_dolzina_roba:      #Če je najdena dolžina več kot polovica celotne.
            rob = rob2                              #Potem je najdeno iskan rob.
            break
        if dolzina_roba/2 > nova_dolzina_roba:      #Drugače odstrani kar je našel.
            for i in range(len(rob)):
                for j in range(len(rob[i])):
                    rob[i][j] -= rob2[i][j] #True-False, True-True dela to kar želimo.
    #END of popravljanje roba.
    
    return visited, rob
    # FOR TESTIN ONLY: ImageDraw module je treba za risanje po sliki nucat. Je
    # zelo počasno tole.
    
    # img_crno = img.convert("RGB")
    # for i in range(xdim):
    #     for j in range(ydim):
    #         if visited[i][j]:
    #             img_crno.putpixel((i, j), (0, 0, 255))
    # img_crno.save('TESTING ONLY-iskanje_lune.png')
    
    # img_rob = img.convert("RGB")
    # for i in range(xdim):
    #     for j in range(ydim):
    #         if rob[i][j]:
    #             img_rob.putpixel((i, j), (255, 0, 0))
    # img_rob.save('TESTING ONLY-iskanje_roba.png')
    

def najdi_kraterje(img):
    pass


def points_to_center(a, b, c):
    """ Dobi tri (x, y) tocke in vrne sredisce kroznice skozi te tocke"""
    xs = map(float, [a[0], b[0], c[0]])
    ys = map(float, [a[1], b[1], c[1]])
    k1 = (ys[1] - ys[0]) / (xs[1] - xs[0])
    k2 = (ys[2] - ys[1]) / (xs[2] - xs[1])

    x = (k1*k2*(ys[0]-ys[2]) + k2*(xs[0]+xs[1]) - k1*(xs[1]+xs[2]))/(2*(k2-k1))
    y =  ((xs[0]+xs[1])/2 - x)/k1 + (ys[0]+ys[1])/2.
    return x, y


def razdalja(a, b):
    return (a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) ** 2


def najdi_sredisce(rob_points, onleft=True):
    """ Funkcija najde sredisce Lune tako, da poisce najvisjo, najnizjo in
    najbolj levo/desno tocko in cez njih fita kroznico
    params:
        rob: 2d boolean array kjer so s True oznacene tocke na robu lune
        onleft: ali je luna cela na levi strani (False, ce je na desni)
    return:
        vrne tuple ((x, y), r), ki oznacujejo sredisce in radij CELE Lune
    """
    
    rob_points = sorted(rob_points, key=lambda x: x[1])
    top = rob_points[-1]
    bottom = rob_points[0]
    rob_points = sorted(rob_points)
    left = rob_points[0]
    if not onleft:
        left = rob_points[-1]

    center = points_to_center(top, bottom, left)
    radij = math.sqrt(razdalja(center, top))
    return center, radij, [top, bottom, left]


def najdi_sredisce2(rob_points):
    """ Funkcija najde sredisce Lune tako da najde tocki ki sta najbolj
    oddaljeni in predpostavi, da sta na kroznici
    params:
        rob: 2d boolean array kjer so s True oznacene tocke na robu lune
    return:
        vrne tuple ((x, y), r), ki oznacujejo sredisce in radij CELE Lune
    """
    
    max_d = 0
    top = None
    bottom = None
    for a in rob_points[::2]:
        for b in rob_points[1::2]:
            d = razdalja(a, b)
            if d > max_d:
                top = a
                bottom = b
                max_d = d
    print len(rob_points)
    print top
    print bottom
    center = (top[0] + bottom[0]) / 2.0, (top[1] + bottom[1]) / 2.0
    return center, math.sqrt(max_d) / 2.0


def najdi_sredisce3(rob_points):
    """ Funkcija najde sredisce Lune s pomocjo triangulacije.
    Veckrat izbere tri tocke na robu, izracuna kaksna kroznica tece skozenj in
    isce cim  manjso kronico, ki zavzame cim vec roba
    params:
        rob: 2d boolean array kjer so s True oznacene tocke na robu lune
    return:
        vrne tuple (x, y), ki oznacujejo sredisce CELE Lune
    """

    rob_points = sorted(rob_points)
    N = len(rob_points)
    rob_points = rob_points[:N/3]
    random.shuffle(rob_points)
    min_razdalja = 12249
    max_cover = 0.3
    min_r = float("inf")
    top_s = None
    extra = None
    for i in range(len(rob_points) - 2):
        a = rob_points[i]
        b = rob_points[i + 1]
        c = rob_points[i + 2]
        if razdalja(a, b) < min_razdalja or \
                razdalja(a, c) < min_razdalja or \
                razdalja(c, b) < min_razdalja:
            continue
        try:
            s = points_to_center(a, b, c)
            r = razdalja(a, s)
            n = len([i for i in rob_points if razdalja(i, s) < r])
            cover = float(n) / N
            if cover > max_cover and min_r > r:
                #max_cover = cover
                min_r = r
                top_s = s
                extra = [a, b, c]
            #print "Not fail"
        except:
            print "Fail"

    print N, max_cover, min_r
    return top_s, math.sqrt(min_r), extra

def najdi_robove(FILE):
    """ Funkcija poisce robove na sliki (že) obdelani
    z edge detect filtrom. Sprejme datoteko slike in
    vrne seznam robovi. V seznamu so po vrsti vsi robovi,
    ki jih zazna.

    Deluje tako da obisce vse tocke. Ce je tocka temna
    gre na naslednjo. Ce ni, sklepa da je tocka del roba
    in zacne iskati okoli nje. Pogleda sosede in jih oznaci
    za del roba ce so priblizno enako svetle. Enako naredi
    za sosede (preveri njihove sosede) dokler jih ne zmanka.
    Sproti oznacuje katere tocke so ze obiskane in jih ne
    preverja 2x.

    Ne zazna popolnoma vseh robov. Ne deluje najhitreje.
    Za nase slike (4000x4000) rabi priblizno 45 sec.
    """

    # Funkcija za primerjanje vrednosti v točki pt z vrednostjo value.
    # Če pt do 10% svetlejša ali temnejša od value vrne True.
    def check(im, pt, value):
        if im.getpixel((pt[0],pt[1]))*1.1 >= value >= im.getpixel((pt[0],pt[1]))*0.9:
            return True
        return False
    
    FILE_NAME, FILE_EXT = os.path.splitext(FILE)
    ima = Image.open("%s" % FILE)
    im = ima.convert("L")
    xdim, ydim = im.size
    robovi = []
    visited = [[False for i in range(ydim)] for j in range(xdim)]
    t = time()

    # Zacnemo kar na zacetku. Lahko bi spremenili da isce samo znotraj
    # ze zaznanega roba Lune.
    for i in range(xdim):
        for j in range(ydim):

##            prc = (i * xdim + (j+1))*10.0 / (xdim * ydim)
##            if int(prc*10)%10==0:
##                print "[",
##                for k in range(int(prc)):
##                    print "#",
##                for k in range(10-int(prc)):
##                    print "-",
##                print "]",
##                print "%s" % round(prc*10, 4)

            val = im.getpixel((i,j))
            # Preveri ce sploh dovolj svetla zacetna tocka
            if val <= 5 or visited[i][j] == True:
                visited[i][j] = True
                continue
            kand = [(i,j)]
            rob = []
            
            while len(kand) > 0:
                I,J = kand.pop()
                if visited[I][J]:
                    continue
                
                visited[I][J] = True
                rob.append((I,J))

                # Preveri sosede
                if I > 0:
                    if check(im, (I-1,J), val) and not visitedGlobal[I-1][J]:
                        kand.insert(0,(I-1,J))
                    if J-1 > 0:
                        if check(im, (I-1,J-1), val) and not visitedGlobal[I-1][J-1]:
                            kand.insert(0,(I-1,J-1))
                    if J+1 < ydim-1:
                        if check(im, (I-1,J+1), val) and not visitedGlobal[I-1][J+1]:
                            kand.insert(0,(I-1,J+1))

                if I+1 < xdim-1:
                    if check(im, (I+1,J), val) and not visitedGlobal[I+1][J]:
                        kand.insert(0,(I+1,J))
                    if J+1 < ydim-1:
                        if check(im, (I+1,J+1), val) and not visitedGlobal[I+1][J+1]:
                            kand.insert(0,(I+1,J+1))
                    if J-1 > 0:
                        if check(im, (I+1,J-1), val) and not visitedGlobal[I+1][J-1]:
                            kand.insert(0,(I+1,J-1))

                if J-1 > 0:
                    if check(im, (I,J-1), val) and not visitedGlobal[I][J-1]:
                        kand.insert(0,(I,J-1))

                if J+1 < ydim-1:
                    if check(im, (I,J+1), val) and not visitedGlobal[I][J+1]:
                        kand.insert(0,(I,J+1))


            # Edge detect zaenkrat najde tudi nepomembne kratke crtice,
            # zato shranimo samo dovolj dolge robove.
            if len(rob) > 20:
                robovi.append(rob)

            visited[i][j] = True

    print "Koncano v %s" % (time() - t)

    # Za vizualno preverjanje obarva vse robove z nakljucnimi barvami
    # in shrani v nov file: file_robovi
    if not ima.mode == 'RGB':
        out = ima.convert('RGB')
    else:
        out = ima

    for i in range(len(robovi)):
        c=int(255*random.random())
        y=int(255*random.random())
        m=int(255*random.random())
        for j in range(len(robovi[i])):
            out.putpixel(robovi[i][j], (c,y,m))
            
    out.save("%s_robovi%s" % (FILE_NAME, FILE_EXT))
    return robovi

def FullHough(img, R_lune, x_lune, y_lune, File):
    """Funkcija poisce kraterje s pomocjo funkcije HoughCircles.
    Poisce jih v 3 razredih nato vse zdruzi. Izloci tiste ki so
    izven lune in tiste ki so preblizu skupaj.
    Sprejme sliko odprto s cv2.imread, radij lune, pozicijo lune
    in ime datoteke kamor shrani rezultat za ogled."""
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    img = cv2.medianBlur(img, 5)

    R = R_lune # Radij lune
    xL = x_lune # Pozicija lune
    yL = y_lune
    
    circles_small = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv2.HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        60,  # how far apart must the radius be (at least)
        param1=51,  # sensitivity of the edge detection
        param2=12,  # sensitivity of the circle detection
        minRadius=0,  # min radius of found circles
        maxRadius=int(25.0 * R / 2795)  # max radius of circles
        )

    circles_med = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv2.HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        70,  # how far apart must the radius be (at least)
        param1=53,  # sensitivity of the edge detection
        param2=12,  # sensitivity of the circle detection
        minRadius=int(26.0 * R / 2795),  # min radius of found circles
        maxRadius=int(50.0 * R / 2795)  # max radius of circles
        )

    circles_big = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv2.HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        70,  # how far apart must the radius be (at least)
        param1=53,  # sensitivity of the edge detection
        param2=13,  # sensitivity of the circle detection
        minRadius=int(51.0 * R / 2795),  # min radius of found circles
        maxRadius=int(80.0 * R / 2795)  # max radius of circles
        )

    # Zdruzi vse kroge v en array
    circles = np.vstack([circles_small[0],circles_med[0],circles_big[0]])
    
    # Preveri ce so kaki preblizu; manj k 60 pix in ce so izven ali na robu lune
    testna = circles.copy()
    for circ1 in circles:
        for circ2 in circles:

            # Najprej pogledamo ce so izven lune ali na robu
            xC2 = circ2[0]
            yC2 = circ2[1]
            rC2 = circ2[2]

            if math.sqrt( razdalja((xC2,yC2),(xL,yL)) ) + rC2  > R:
                to_be_removed = circ2
                other_rows = (testna != to_be_removed).any(axis=1)
                testna = testna[other_rows]
                continue
            
            # Zdaj pogledamo ce so preblizu
            xC1 = circ1[0]
            yC1 = circ1[1]
            rC1 = circ1[2]
            
            dist=math.sqrt( razdalja((xC1,yC1),(xC2,yC2)) )
            adapMinDist = 60.0 * R / 2795
            
            if dist < adapMinDist and xC1 != xC2 and yC1 != yC2 and rC1 != rC2:
                if rC1 > rC2:
                    to_be_removed = circ2
                    other_rows = (testna != to_be_removed).any(axis=1)
                    testna = testna[other_rows]
                else:
                    to_be_removed = circ1
                    other_rows = (testna != to_be_removed).any(axis=1)
                    testna = testna[other_rows]

    testna = np.uint16(np.around(testna))
    
    for i in testna:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 1)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 1, (0, 0, 255), 2)

    cv2.imwrite('img/%s_FullHough.jpg' % File, cimg)
    
    return len(testna)
