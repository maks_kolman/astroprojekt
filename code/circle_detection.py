import cv2
import cv2.cv as cv
import numpy as np

SHOW_SMALL = True
SHOW_MEDIUM = False
SHOW_LARGE = False

img = cv2.imread('img/croped1.jpg', 0)
img = cv2.medianBlur(img, 5)
cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

print "Converted to grayscale and searching for circles!"

if SHOW_SMALL:
    #  Find small circles
    print "Finding small circles"
    circles_small = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv.CV_HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        5,  # how far apart must the radius be (at least)
        param1=40,  # sensitivity of the edge detection
        param2=20,  # sensitivity of the circle detection
        minRadius=0,  # min radius of found circles
        maxRadius=20  # max radius of circles
    )
    circles_small = np.uint16(np.around(circles_small))
    for i in circles_small[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 1)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 1, (0, 0, 255), 2)
if SHOW_MEDIUM:
    #  Find medium circles
    print "Finding medium circles"
    circles_medium = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv.CV_HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        10,  # how far apart must the radius be (at least)
        param1=50,  # sensitivity of the edge detection
        param2=22,  # sensitivity of the circle detection
        minRadius=20,  # min radius of found circles
        maxRadius=40  # max radius of circles
    )
    circles_medium = np.uint16(np.around(circles_medium))

    for i in circles_medium[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (255, 0, 0), 1)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 1, (0, 0, 255), 1)

if SHOW_LARGE:
    #  Find large circles
    print "Finding large circles"
    circles_large = cv2.HoughCircles(
        img,  # 8-bit grayscale image
        cv.CV_HOUGH_GRADIENT,  # method to use (only option)
        1,  # image scaling
        30,  # how far apart must the radius be (at least)
        param1=50,  # sensitivity of the edge detection
        param2=23,  # sensitivity of the circle detection
        minRadius=40,  # min radius of found circles
        maxRadius=60  # max radius of circles
    )
    circles_large = np.uint16(np.around(circles_large))

    for i in circles_large[0, :]:
        # draw the outer circle
        cv2.circle(cimg, (i[0], i[1]), i[2], (0, 0, 255), 1)
        # draw the center of the circle
        cv2.circle(cimg, (i[0], i[1]), 1, (0, 0, 255), 1)
cv2.imwrite('img/output.jpg', cimg)
#cv2.imshow('detected circles', cimg)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
