# Velikost kraterjev na Luni

Projekt pri predmetu Astronimija II

## Struktura:
 * img/ - Lokacija slik iz opazovanj in arhiva za analizo
 * porocilo/ - Lokacija za porocilo pisano v LaTeXu -> pdf
 * code/ - Lokacija vseh phython script, modulov, knjiznic ipd.
 * analyze.py - Ta datoteka naj uporablja code/ in img/ za analizo
