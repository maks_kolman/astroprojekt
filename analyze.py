from time import time

from PIL import Image, ImageDraw

import code

PREBERI_ROB_IZ_DATOTEKE = False
FILE_NAME = 'half-moon-2'


# ========== Program ============
t = time()
print "Odpiram sliko %s in convertam v B/W" % FILE_NAME
im = Image.open("img/%s.jpg" % FILE_NAME)
crnobela = im.convert("L")
print "Done in %s" % (time() - t)


if not PREBERI_ROB_IZ_DATOTEKE:
    t = time()
    print "Iscem rob"
    crno, rob = code.najdi_luno(crnobela)
    print "Done in %s" % (time() - t)
else:
    t = time()
    print "Loading rob from file..."
    exec (open("rob").read())
    print "Done in %s" % (time() - t)



t = time()
print "Iscem sredino Lune"
rob_points = []
for i, line in enumerate(rob):
    for j, field in enumerate(line):
        if field:
            rob_points.append((i, j))

center, r, extra = code.najdi_sredisce(rob_points)
print "Done in %s" % (time() - t)


x, y = center
draw = ImageDraw.Draw(im)
draw.ellipse((x - r, y - r, x + r, y + r))
draw.ellipse((x - 10, y - 10, x + 10, y + 10), fill=(0, 0, 0))
for i in extra:
    draw.ellipse((i[0]-10, i[1]-10, i[0]+10, i[1]+10))
for x in rob_points:
    im.putpixel(x, (255, 0, 0))
im.save("img/%s_output.jpg" % FILE_NAME)
